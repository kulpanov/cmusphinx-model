#!/bin/bash

if true; then
pocketsphinx_batch \
    -hmm model/map \
    -lm  msu_ru_zero.lm.dmp \
    -dict msu_ru_zero_train.dic \
    -cepdir voices \
    -cepext .raw \
    -ctl voices/arctic22.listoffiles  \
    -hyp result.hyp\
    -logfn sphinx.log \
    -adcin yes \
    -mllr mllr_matrix \
    -samprate 8000
fi 

CUR_DIR=`pwd`
/usr/local/lib/sphinxtrain/scripts/decode/word_align.pl $CUR_DIR/voices/arctic22.transcription $CUR_DIR/result.hyp >result.log