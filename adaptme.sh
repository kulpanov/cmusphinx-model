#!/bin/sh

#модель тренировки
MODEL=model
#каталог с корпусом тренировки
VOICE_DIR=voices
#Базовый файл корпуса тренировки
ARCTIC=arctic22 
#Словарь языка
DICTONARY=msu_ru_zero_train.dic 
#sphinx train
SPHINX_TRAIN=/usr/local/libexec/sphinxtrain
#SAMPRATE
SAMPRATE=8000


while true; do
#get mdef.txt from model - optional, if need
pocketsphinx_mdef_convert -text $MODEL/src/mdef $MODEL/src/mdef.txt 2> adaptme.log
if ! [ $? -eq 0 ];  then 
  echo pocketsphinx_mdef_convert: error was occured, see apadtme.log
  break
fi

cp -f $MODEL/src/* $MODEL/mllr
cp -f $MODEL/src/* $MODEL/map

#wav->mfc prepare acoustic data
echo fe
sphinx_fe -argfile $MODEL/src/feat.params \
      -samprate $SAMPRATE \
      -c $VOICE_DIR/$ARCTIC.listoffiles \
      -di $VOICE_DIR \
      -do . -ei raw -eo mfc -raw yes 2>> adaptme.log
if ! [ $? -eq 0 ];  then 
  echo sphinx_fe: error was occured, see apadtme.log
  break
fi

#get statistics
echo bw
$SPHINX_TRAIN/bw -hmmdir $MODEL/src -moddeffn $MODEL/src/mdef.txt \
      -ts2cbfn .cont. \
      -feat 1s_c_d_dd \
      -cmn current -agc none \
      -lda  $MODEL/src/feature_transform \
      -dictfn $DICTONARY \
      -ctlfn $VOICE_DIR/$ARCTIC.listoffiles \
      -lsnfn $VOICE_DIR/$ARCTIC.transcription -accumdir . 2>> adaptme.log
if ! [ $? -eq 0 ] ; then 
  echo bw: error was occured, see apadtme.log
  break  
fi
#break
echo mllr_solve
$SPHINX_TRAIN/mllr_solve \
  -meanfn $MODEL/src/means \
  -varfn $MODEL/src/variances \
  -outmllrfn mllr_matrix \
  -accumdir .  2>> adaptme.log
if ! [ $? -eq 0 ];  then 
  echo mllr_solve: error was occured, see apadtme.log
  break
fi

if true; then
echo mllr_transform
$SPHINX_TRAIN/mllr_transform \
   -inmeanfn  $MODEL/src/means  \
   -outmeanfn $MODEL/mllr/means \
   -mllrmat mllr_matrix \
   -moddeffn $MODEL/src/mdef -cdonly yes 2>> adaptme.log

if ! [ $? -eq 0 ];  then 
  echo mllr_transform: error was occurred, see apadtme.log
  break
fi

#get statistics again, after MLLR transform
$SPHINX_TRAIN/bw -hmmdir $MODEL/mllr -moddeffn $MODEL/mllr/mdef.txt \
      -ts2cbfn .cont. \
      -feat 1s_c_d_dd \
      -cmn current -agc none \
      -dictfn $DICTONARY \
      -ctlfn $VOICE_DIR/$ARCTIC.listoffiles \
      -lsnfn $VOICE_DIR/$ARCTIC.transcription -accumdir . 2>> adaptme.log
if ! [ $? -eq 0 ] ; then 
  echo bw: error was happend, see apadtme.log
  break  
fi

fi

#break
#make map adaptation
echo map_adapt
$SPHINX_TRAIN/map_adapt \
   -meanfn $MODEL/mllr/means \
   -varfn  $MODEL/mllr/variances\
   -mixwfn $MODEL/mllr/mixture_weights \
   -tmatfn $MODEL/mllr/transition_matrices \
   -tau 310 \
   -accumdir . \
   -mapmeanfn $MODEL/map/means \
   -mapvarfn $MODEL/map/variances \
   -mapmixwfn $MODEL/map/mixture_weights \
   -maptmatfn $MODEL/map/transition_matrices 2>> adaptme.log

if ! [ $? -eq 0 ];  then 
  echo map_adapt: error was happend, see apadtme.log
  break
fi

break  
done

rm ./*.mfc ./*_counts;

